from tkinter import *


class GUI(object):
    def __init__(self, size, objects, difficulty):
        if size % 2 != 1:
            self.size = size + 1
        else:
            self.size = size
        self.objects = objects
        self.difficulty = difficulty

        self.wl = 700 / self.size

        self.buttons = []
        self.list = []

        self.pickX = ""
        self.pickY = ""

        self.object = True
        self.objectPlace = None
        self.box = False

        self.erlaubnis1 = None
        self.erlaubnis2 = None
        self.erlaubnis3 = None
        self.erlaubnis4 = None
        self.erlaubnis5 = None
        self.erlaubnis6 = None
        self.erlaubnis7 = None
        self.erlaubnis8 = None

    def createGUI(self):
        self.fenster = Tk()
        self.fenster.title("Minigame")
        self.fenster.geometry("700x700")

        # Frame

        self.frameFelder = Frame(master=self.fenster, background="black")
        self.frameFelder.place(x=0, y=0, width=700, height=700)

        # Feld erstellen

        for i in range(self.size):
            x = 0
            y = i * self.wl

            for a in range(self.size):
                self.feld = Cell(x, y, self.fenster, self.wl,
                                 self.button_press)
                x += self.wl
                self.list.append(self.feld)

            self.buttons.append(self.list)

        # Commands

    def button_press(self, x, y):

        # Objekt hinlegen
        if self.object == True and self.box == False:
            self.object = False
            self.objectPlace = [x, y]
            self.pickX = self.buttons[x]
            self.pickY = self.buttons[y]
            self.pickY.button.config(background="green")


class Cell(object):
    def __init__(self, x, y, fenster, wl, command):
        self.x = int(x / 100)
        self.y = int(y / 100)
        self.fenster = fenster
        self.wl = wl
        self.command = command

        self.button = Button(master=self.fenster,
                             background="grey",
                             command=self.command(self.x, self.y))
        self.button.place(x=x, y=y, width=self.wl, height=self.wl)

    def getXY(self):
        return [self.x, self.y]
